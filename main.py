from flask import Flask, render_template, request
app = Flask(__name__)
@app.route('/form')
def form():
    return render_template('form.html')

@app.route('/submitted', methods=['POST'])
def submitted_form():
    link = request.form['link']
    news = request.form['news']
    return render_template(
    'submitted_form.html',
    link=link,
    news=news)

